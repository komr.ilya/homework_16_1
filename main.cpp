//
//  main.cpp
//  homework_16
//
//  Created by Ilya Komrakov on 28.04.2020.
//  Copyright © 2020 Ilya Komrakov. All rights reserved.
//

#include <iostream>
#include <ctime>

/*
PRIORITY:
long double
double
float
unsigned long long
long long
unsigned long
long
unsigned int
int
*/


int main()
{
    const int N = 2;

    int arrN[N][N];

    // Fill the array
    for (int i = 0; i < N; i++)
    {
        for (int j = 0; j < N; j++)
        {
            arrN[i][j] = i + j;
        }
        
    }

    // Print elements of array
    for (int i = 0; i < N; i++)
    {
        for (int j = 0; j < N; j++)
        {
            std::cout << arrN[i][j] << std::endl;
        }
        
    }

    

    // Set current day
    time_t seconds = time(NULL);
    tm* timeinfo = localtime(&seconds);
    int day = timeinfo->tm_mday;


    // Print sum elements of array string
    for (int i = 0; i < N; i++)
    {
        if ( (day%N) == i )
        {
            int SumArrayString = 0;

            for (int j = 0; j < N; j++)
            {
                SumArrayString += arrN[i][j];
            }

            std::cout << "\n" << SumArrayString << std::endl;
            
        }

        continue;
        
    }
    
    
    return 0;
}
